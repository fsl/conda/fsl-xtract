if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper xtract xtract_viewer \
      xtract_stats xtract_blueprint xtract_qc create_blueprint get_wgb \
      make_subcort_bp xtract_divergence
fi
