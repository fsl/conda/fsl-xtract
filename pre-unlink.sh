if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper xtract xtract_viewer \
      xtract_stats xtract_blueprint xtract_qc create_blueprint get_wgb \
      make_subcort_bp xtract_divergence
fi
